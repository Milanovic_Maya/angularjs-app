var myNinjaApp = angular.module('myNinjaApp', ['ngRoute', 'ngAnimate']);

myNinjaApp.config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider) {
    // runs before application starts
    // routing - before anything starts

    $locationProvider.html5Mode(true);

    $routeProvider
        // when user visits particular url, do something 
        .when(
            '/home', {
                templateUrl: 'views/home.html',
                controller: 'NinjaController'
            })
        .when(
            '/directory', {
                templateUrl: 'views/directory.html',
                // which controller is responsible for this view
                controller: 'NinjaController'
            })
        .when(
            '/contact', {
                templateUrl: 'views/contact.html',
                // which controller is responsible for this view
                controller: 'ContactController'
            })
        .when(
            '/contact-success', {
                templateUrl: 'views/contact-success.html',
                // which controller is responsible for this view
                controller: 'ContactController'
            })
        .otherwise({
            redirectTo: '/home'
        })
}]);




myNinjaApp.run(function () {
    // runs when does the application too
});




// define custom directive

myNinjaApp.directive('randomNinja', [function () {
    // define properties of directive
    return {
        // restrict directive to be used in certain way
        restrict: 'E',
        // pass data through, create isolated scope, directive will have own scope
        scope: {
            // bind data passed via html attribute ninjas
            ninjas: "=",
            title: "="
        },
        // output on the page whatever is in template
        templateUrl: 'views/random.html',
        transclude: true,
        replace: true,
        controller: function ($scope) {
            $scope.random = Math.floor(Math.random() * 4);
        }
    };
}]);




myNinjaApp.controller('NinjaController', ['$scope', '$http', function ($scope, $http) {
    $scope.addNinja = function ($event) {
        console.log($event);
        $event.preventDefault();

        $scope.ninjas.push({
            name: $scope.newNinja.name,
            belt: $scope.newNinja.belt,
            rate: parseInt($scope.newNinja.rate),
            available: true
        })

        $scope.newNinja.name = '';
        $scope.newNinja.belt = '';
        $scope.newNinja.rate = '';
    };

    $scope.removeNinja = function (ninja) {
        var removedNinja = $scope.ninjas.indexOf(ninja);
        $scope.ninjas.splice(removedNinja, 1);

        // $scope.ninjas=$scope.ninjas.filter(ninjaItem=>ninjaItem.name!==ninja.name)
    }

    $scope.removeAll = function ($event) {
        $scope.ninjas = [];
    };

    // get data from path we pass
    $http.get('data/ninjas.json').then(function (ninjas) {
        $scope.ninjas = ninjas.data
    })

}]);




myNinjaApp.controller('ContactController', ['$scope', '$location', function ($scope, $location) {
    $scope.sendMessage = function ($event) {
        console.log($event);
        // $event.preventDefault();

      $location.path('contact-success');
    };


}]);